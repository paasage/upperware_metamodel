/**
 */
package eu.paasage.upperware.metamodel.cp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Domain</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.paasage.upperware.metamodel.cp.CpPackage#getBooleanDomain()
 * @model
 * @generated
 */
public interface BooleanDomain extends Domain {
} // BooleanDomain
