/**
 * Copyright (C) 2015 INRIA, Université Lille 1
 *
 * Contacts: daniel.romero@inria.fr laurence.duchien@inria.fr & lionel.seinturier@inria.fr
 * Date: 09/2015
 
 * This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this 
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package eu.paasage.upperware.metamodel.types.util;

import eu.paasage.upperware.metamodel.types.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see eu.paasage.upperware.metamodel.types.TypesPackage
 * 
 */
public class TypesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 */
	protected static TypesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 */
	public TypesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TypesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * 
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 */
	protected TypesSwitch<Adapter> modelSwitch =
		new TypesSwitch<Adapter>() {
			@Override
			public Adapter caseValueUpperware(ValueUpperware object) {
				return createValueUpperwareAdapter();
			}
			@Override
			public Adapter caseNumericValueUpperware(NumericValueUpperware object) {
				return createNumericValueUpperwareAdapter();
			}
			@Override
			public Adapter caseIntegerValueUpperware(IntegerValueUpperware object) {
				return createIntegerValueUpperwareAdapter();
			}
			@Override
			public Adapter caseLongValueUpperware(LongValueUpperware object) {
				return createLongValueUpperwareAdapter();
			}
			@Override
			public Adapter caseFloatValueUpperware(FloatValueUpperware object) {
				return createFloatValueUpperwareAdapter();
			}
			@Override
			public Adapter caseDoubleValueUpperware(DoubleValueUpperware object) {
				return createDoubleValueUpperwareAdapter();
			}
			@Override
			public Adapter caseStringValueUpperware(StringValueUpperware object) {
				return createStringValueUpperwareAdapter();
			}
			@Override
			public Adapter caseBooleanValueUpperware(BooleanValueUpperware object) {
				return createBooleanValueUpperwareAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * 
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link eu.paasage.upperware.metamodel.types.ValueUpperware <em>Value Upperware</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see eu.paasage.upperware.metamodel.types.ValueUpperware
	 * 
	 */
	public Adapter createValueUpperwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link eu.paasage.upperware.metamodel.types.NumericValueUpperware <em>Numeric Value Upperware</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see eu.paasage.upperware.metamodel.types.NumericValueUpperware
	 * 
	 */
	public Adapter createNumericValueUpperwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link eu.paasage.upperware.metamodel.types.IntegerValueUpperware <em>Integer Value Upperware</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see eu.paasage.upperware.metamodel.types.IntegerValueUpperware
	 * 
	 */
	public Adapter createIntegerValueUpperwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link eu.paasage.upperware.metamodel.types.LongValueUpperware <em>Long Value Upperware</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see eu.paasage.upperware.metamodel.types.LongValueUpperware
	 * 
	 */
	public Adapter createLongValueUpperwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link eu.paasage.upperware.metamodel.types.FloatValueUpperware <em>Float Value Upperware</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see eu.paasage.upperware.metamodel.types.FloatValueUpperware
	 * 
	 */
	public Adapter createFloatValueUpperwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link eu.paasage.upperware.metamodel.types.DoubleValueUpperware <em>Double Value Upperware</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see eu.paasage.upperware.metamodel.types.DoubleValueUpperware
	 * 
	 */
	public Adapter createDoubleValueUpperwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link eu.paasage.upperware.metamodel.types.StringValueUpperware <em>String Value Upperware</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see eu.paasage.upperware.metamodel.types.StringValueUpperware
	 * 
	 */
	public Adapter createStringValueUpperwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link eu.paasage.upperware.metamodel.types.BooleanValueUpperware <em>Boolean Value Upperware</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see eu.paasage.upperware.metamodel.types.BooleanValueUpperware
	 * 
	 */
	public Adapter createBooleanValueUpperwareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * 
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TypesAdapterFactory
